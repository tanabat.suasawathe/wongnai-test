const express = require('express')
const app = express()
var redis = require('redis');

// Add this to the VERY top of the first file loaded in your app
var apm = require('elastic-apm-node').start({

  // Override the service name from package.json
  // Allowed characters: a-z, A-Z, 0-9, -, _, and space
  serviceName: 'Test_APM',

  // Use if APM Server requires a secret token
  secretToken: '',

  // Set the custom APM Server URL (default: http://localhost:8200)
  serverUrl: 'http://10.45.231.55:8200',

  // Set the service environment
  environment: 'production'
})

app.get('/hello1', function (req, res) {
   writeSession()
   res.send("Hello-1")
})

app.listen(8000, () => console.log('Hello-1 listening on port 8000!'))

function writeSession() {
  var dt = new Date();
  var utcDate = dt.toUTCString();
  var client = redis.createClient(process.env.port,'redis');
  client.on('connect', function() { console.log('Redis client connected') })
  client.on('error', function(err) { console.log('Something went wrong ' + err) })
  client.get('lastest_session_id', function (error, id) {
    if (error) {
        console.log(error);
        throw error;
    }
    if(id == null) {
    	id = 1
    } else {
    	id = Number(id) + 1
    }
    client.set('lastest_session_id', id , redis.print);
 	  client.set(id, utcDate ,redis.print);
 	  console.log(id)
  })
}