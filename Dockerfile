FROM node:16-alpine3.11 as hello-1
COPY package.json ./
RUN npm install
COPY app/hello-1.js .
CMD ["npm","start"]
EXPOSE 8000

FROM node:16-alpine3.11 as hello-2
COPY package2.json ./package.json
RUN npm install
COPY app/hello-2.js .
CMD ["npm","start"]
EXPOSE 8000


