## Test app ##
# Build docker
docker build -t app-hello-world1 --target hello-1 .

docker build -t app-hello-world2 --target hello-2 .
# Check docker images
docker ps 
should have 2 docker images = app-hello-world1,app-hello-world2
# Test running app #
docker-compose up 
# Test access app 
curl localhost:8080/hello1   >>> it should return message "Hello-1"
curl localhost:8080/hello2  >>>  it should return message "Hello-2"
# Clean up 
docker-compose down





# example-nodejs
Just simple nodejs application

## Installation ##
npm install express

npm install redis

## hello-1 ##

In case of hello-1 , it need to have redis server to keep session & timestamp

export port=redis_port

export host=redis_host

node app/hello-1.js 

## hello-2 ##

node app/hello-2.js
